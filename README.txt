*******************************************************
     README.txt for formupdater.module for Drupal
     by Jeff Robbins :: jeff /@t\ jjeff /d0t\ com
*******************************************************

Let's face it, upgrading modules to the new formapi in
Drupal 4.7 can be quite a challenge. Form Updater can
help developers with the upgrade by searching through
old Drupal code to find calls to old form functions
(such as form_textfield, form_radios, etc). It will then
reformat these functions as arrays for use with the new
Drupal 4.7 formapi. It's far from a complete solution,
but it provides a big kick-start for the upgrade.